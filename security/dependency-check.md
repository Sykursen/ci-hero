# Dependency-Check

## Gitlab-CI

```yaml
stages:
    - dependency

dependency-check:
    stage: dependency
    image:
        name: owasp/dependency-check
        entrypoint: [""]
    script:
        - /usr/share/dependency-check/bin/dependency-check.sh --project Test --out . --scan . --enableExperimental --failOnCVSS 7
    allow_failure: true
    artifacts:
      untracked: true
```

## Drone-CI

> ToDo
