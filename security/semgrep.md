# Semgrep

## Gitlab-CI

```yaml
stages:
    - application-sec

semgrep-global-sec:
  stage: application-sec
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --gitlab-json > semgrep-global-sec.json || true
  allow_failure: true
  only:
    - master
  variables:
    SEMGREP_RULES: >- # more at semgrep.dev/explore
      p/owasp-top-ten      
      p/security-audit
      p/jwt
      p/command-injection
      p/sql-injection
      p/xss
      p/insecure-transport
      p/r2c-ci
  artifacts:
    reports:
      sast: semgrep-global-sec.json

semgrep-java-sec:
  stage: application-sec
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --gitlab-json > semgrep-java.json || true
  allow_failure: true
  only:
    - master
  variables:
    SEMGREP_RULES: >- # more at semgrep.dev/explore
      p/java
  artifacts:
    reports:
      sast: semgrep-java.json

semgrep-javascript-sec:
  stage: application-sec
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --gitlab-json > semgrep-javascript.json || true
  allow_failure: true
  only:
    - master
  variables:
    SEMGREP_RULES: >- # more at semgrep.dev/explore
      p/javascript
      p/typescript
      p/eslint-plugin-security
      p/clientside-js
      p/electron-desktop-app
  artifacts:
    reports:
      sast: semgrep-javascript.json

semgrep-php-sec:
  stage: application-sec
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --gitlab-json > semgrep-php.json || true
  allow_failure: true
  only:
    - master
  variables:
    SEMGREP_RULES: >- # more at semgrep.dev/explore
      p/phpcs-security-audit
  artifacts:
    reports:
      sast: semgrep-php.json

semgrep-python-sec:
  stage: application-sec
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --gitlab-json > semgrep-python.json || true
  allow_failure: true
  only:
    - master
  variables:
    SEMGREP_RULES: >- # more at semgrep.dev/explore
      p/python
      p/flask
      p/django
      p/trailofbits
      p/gitlab-bandit
  artifacts:
    reports:
      sast: semgrep-python.json

semgrep-secrets:
  stage: application-sec
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --gitlab-json > semgrep-secrets.json || true
  allow_failure: true
  only:
    - master
  variables:
    SEMGREP_RULES: >- # more at semgrep.dev/explore
      p/secrets
  artifacts:
    reports:
      sast: semgrep-secrets.json
```

## Drone-CI

> ToDo
